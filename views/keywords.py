from datetime import datetime
from typing import Optional
from database import Base
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column


class Keyword(Base):

    __tablename__ = "keywords"

    id: Mapped[int] = mapped_column(primary_key=True)
    title: Mapped[Optional[str]]

    created_at: Mapped[datetime] = mapped_column(default=datetime.utcnow())
    updated_at: Mapped[datetime] = mapped_column(default=datetime.utcnow(),
                                                 onupdate=func.now())
    deleted_at: Mapped[Optional[datetime]]
