from datetime import datetime
from typing import Optional
from database import Base
from sqlalchemy.sql import func
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column


class KeywordPositions(Base):

    __tablename__ = "keywords_positions"

    id: Mapped[int] = mapped_column(primary_key=True)
    keyword_id: Mapped[int] = mapped_column(ForeignKey("keywords.id"))
    product_id: Mapped[int] = mapped_column(ForeignKey("products.id"))

    page: Mapped[Optional[str]]
    position: Mapped[Optional[int]]
    date: Mapped[Optional[datetime]]

    created_at: Mapped[datetime] = mapped_column(default=datetime.utcnow())
    updated_at: Mapped[datetime] = mapped_column(default=datetime.utcnow(),
                                                 onupdate=func.now())
    deleted_at: Mapped[Optional[datetime]]
