from datetime import datetime
from typing import Optional
from database import Base
from sqlalchemy import ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column


class MainMuneСategotys(Base):

    __tablename__ = "main_mune_categotys"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    id_category: Mapped[Optional[int]]
    isDenyLink: Mapped[Optional[bool]]
    landing: Mapped[Optional[bool]]
    name: Mapped[str]
    parent: Mapped[Optional[str]]
    query: Mapped[Optional[str]]
    seo: Mapped[Optional[str]]
    shard: Mapped[Optional[str]]
    url: Mapped[str]
    deep: Mapped[int]

    created_at: Mapped[datetime] = mapped_column(default=datetime.utcnow())
    updated_at: Mapped[datetime] = mapped_column(default=datetime.utcnow(),
                                                 onupdate=func.now())
    deleted_at: Mapped[Optional[datetime]]

