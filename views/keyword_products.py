from datetime import datetime
from typing import Optional
from database import Base
from sqlalchemy import ForeignKey
from sqlalchemy.orm import Mapped, mapped_column
from sqlalchemy.sql import func

from views.keywords import Keyword


class KeywordProducts(Base):

    __tablename__ = "keywords_products"

    id: Mapped[int] = mapped_column(primary_key=True)
    keyword_id: Mapped[int] = mapped_column(ForeignKey("keywords.id"))
    product_id: Mapped[int] = mapped_column(ForeignKey("products.id"))

    created_at: Mapped[datetime] = mapped_column(default=datetime.utcnow())
    updated_at: Mapped[datetime] = mapped_column(default=datetime.utcnow(),
                                                 onupdate=func.now())
    deleted_at: Mapped[Optional[datetime]]
