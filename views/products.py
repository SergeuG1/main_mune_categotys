from datetime import datetime
from typing import Optional
from database import Base
from sqlalchemy import ForeignKey
from sqlalchemy.sql import func
from sqlalchemy.orm import Mapped, mapped_column


class Product(Base):

    __tablename__ = "products"

    id: Mapped[int] = mapped_column(primary_key=True, index=True)
    seller_id: Mapped[Optional[int]] = mapped_column(ForeignKey("sellers.id"),
                                                     index=True)
    name: Mapped[Optional[str]]
    wb_article: Mapped[Optional[int]] = mapped_column(index=True)
    nomenclature: Mapped[Optional[str]]
    in_ads: Mapped[bool]
    created_at: Mapped[datetime] = mapped_column(default=datetime.utcnow())
    updated_at: Mapped[datetime] = mapped_column(default=datetime.utcnow(),
                                                 onupdate=func.now())
    deleted_at: Mapped[Optional[datetime]]
