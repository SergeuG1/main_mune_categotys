import math
from random import randint
from time import sleep
import time
import aiohttp
from loguru import logger
from asyncio import BoundedSemaphore, gather, Semaphore
from aiohttp import ClientSession
import requests


async def parse_page(session: ClientSession,
                     page: int,
                     category_brand,
                     brand_id: int
                     ) -> list[dict]:

    params = {
        'appType': '1',
        'cat': category_brand[1],
        'curr': 'rub',
        'dest': '123585728',
        'fbrand': brand_id,
        'page': page,
        'regions': '80,38,83,4,64,33,68,70,30,40,86,69,1,31,66,110,48,22,114',
        'sort': 'popular',
        'spp': '33',
        'uclusters': '0'
    }
    
    url = f'https://catalog.wb.ru/catalog/{category_brand[0]}/catalog'

    for i in range(0, 5):
        try:
            response = await session.get(url=url, params=params)
        except Exception as e:
            logger.warning(e)
            continue

        if response.status != 200:
            sleep(0.5)
            continue
        result = await response.json(content_type=response.content_type)

        products = result.get('data', {}).get('products', [])

        return products


async def postion_products(session, category_brand, brands: dict) -> list[dict]:

    result = {}
    for idx, brand in enumerate(brands):
        logger.info(f"{idx + 1} из {len(brands)}")
        brand_id = brand["id"]

        result[brand["id"]] = [] 

        pages = math.ceil(brand["count"] / 100)

        connector = aiohttp.TCPConnector()
        tasks = [
            parse_page(session, page, category_brand, brand["id"])
            for page in range(1, pages + 1)
        ]
        temp = await gather(*tasks)

        for _ in temp:
            if _ is not None:
                 result[brand_id].extend(_)
            else: 
                break

        logger.success(f"Было собрано {len(result[brand_id])} -- {category_brand}")

    return result

