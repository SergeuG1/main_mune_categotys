import asyncio
import time
import aiohttp
import requests
from loguru import logger
from datetime import datetime, timedelta
from database import SessionLocal
from parse import postion_products
from views.products import Product
from views.main_mune_categotys import MainMuneСategotys

USER_AGENT = "Mozilla/5.0 (Windows NT 10.0, Win64, x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/108.0.0.0 Safari/537.36 OPR/94.0.0.0"  # noqa 501
name_date = datetime.now().strftime("%d_%m_%Y_%H_%M.log")


# def error_handler(exception: BaseException):

#     BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
#     CHAT_ID = -935052991
#     file_name = datetime.now().strftime(name_date)

#     data = {'chat_id': CHAT_ID, "caption": "🔴 КРИТИЧЕСКАЯ ОШИБКА\nМодуль: NiiN сбор категорий товаров"}
#     logger.stop()
#     url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
#     with open('logs/' + file_name, '') as f:
#         files = {'document': f}
        # requests.post(url, data=data, files=files)


# def send_log_to_tg():

#     BOT_TOKEN = '6217920753:AAEeWlLEbMZduA4uilPYS3VHEeFjgFIL5YI'
#     CHAT_ID = -935052991
#     file_name = datetime.now().strftime(name_date)

#     data = {'chat_id': CHAT_ID, "caption": "🟢 Отчёт по логам\nМодуль: NiiN сбор категорий товаров"}
#     logger.stop()
#     url = 'https://api.telegram.org/bot{}/sendDocument'.format(BOT_TOKEN)
#     with open('logs/' + file_name, 'rb') as f:
#         files = {'document': f}
        # requests.post(url, data=data, files=files)


def get_categorys() -> list[dict]:
    while True:
        response = requests.get('https://static-basket-01.wb.ru/vol0/data/main-menu-ru-ru-v2.json')
        if response.status_code == 200:
            categorys = response.json()
            return categorys
        else:
            time.sleep(1)


def get_brands(list_categorys: list) -> list:
    with SessionLocal() as session:
        try:
            deep = 0
            for sublist in list_categorys:
                if "childs" in sublist:
                    for element in get_brands(sublist["childs"]):
                        yield element
                else:
                    depth_id_above = sublist["id"]
                    category = MainMuneСategotys(
                        id_category = depth_id_above,
                        isDenyLink = sublist.get("isDenyLink", False),
                        landing = sublist.get("landing", False),
                        name = sublist.get("name"),
                        parent = sublist.get("parent", 0),
                        query = sublist.get("query"),
                        seo = sublist.get("seo"),
                        shard = sublist.get("shard"),
                        url = sublist.get("url"),
                        deep = deep,
                        created_at = datetime.now(),
                        updated_at = datetime.now(),
                        deleted_at = None,
                    )
                    session.add(category)
                    session.commit()
                    yield sublist
        except TypeError:
            yield list_categorys


def get_sub_brands(list_brands: list) -> dict:

    all_brands = {(brand["shard"], brand["id"]): [] for brand in list_brands if "shard" in brand}
    for idx, brand in enumerate(list_brands):
        params = {
            "filters": "fbrand",
            "cat": brand["id"],
            "appType": 1,
            "curr": "rub",
            "dest": 123585728,
            "regions": "80,38,83,4,64,33,68,70,30,40,86,69,1,31,66,110,48,22,114",
            "spp": 33,
            "uclusters": 0,
        }
        response = requests.get(f'https://catalog.wb.ru/catalog/{brand["shard"]}/v4/filters', params=params)
 
        if response.text != '':
            for filter in response.json()["data"]["filters"]:
                all_brands[(brand["shard"], brand["id"])].extend(filter["items"])
            logger.info(f"{idx} из {len(list_brands)}")
        else:
            continue

    return all_brands


async def start():
    with SessionLocal() as session:
        session.query(MainMuneСategotys).delete()
        session.commit()

    categorys = get_categorys()
    sub_brands = list(get_brands(categorys))
    all_brands = get_sub_brands(sub_brands[::])
    connector = aiohttp.TCPConnector()
    async with aiohttp.ClientSession(connector=connector) as session:
        tasks = [
            postion_products(session, categorys_brand, brands[::])
            for categorys_brand, brands in all_brands.items()
        ]
        temp = await asyncio.gather(*tasks)
        
    res = list(zip(all_brands.keys(), temp))
    return res
    # send_log_to_tg()


if __name__ == "__main__":

    logger.add(
        "logs/{}".format(datetime.now().strftime("%d_%m_%Y.log")),
        format="{time} {level} {message}",
        level="DEBUG")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(start())

